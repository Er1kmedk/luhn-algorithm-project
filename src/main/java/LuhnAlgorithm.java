import java.util.Scanner;

public class LuhnAlgorithm {

    private static int expectedCheckDigit = 0;

    public static void main(String[] args) {
        Scanner inputScanner = new Scanner(System.in);
        System.out.println("Welcome to the Luhn Algorithm Application!\nPlease enter the number:");
        String userInput = inputScanner.nextLine();

        int length = userInput.length();
        boolean isLegit = luhnCheck(userInput);

        System.out.println("Input: " + formatInput(userInput));
        System.out.println("Provided: " + userInput.charAt(length -1));
        System.out.println("Expected: " + expectedCheckDigit);

        if (isLegit) {
            System.out.println("Checksum: Valid");
        }
        else {
            System.out.println("Checksum: Invalid");
        }

        if (length == 16 && isLegit) {
            System.out.println("Digits: " + length + " (Credit card)");
        }
        else if (length == 10 && isLegit) {
            System.out.println("Digits: " + length + " (Person number)");
        }
        else {
            System.out.println("Digits: " + length + " (Humbug number)");
        }
    }

    // A method for formatting the user input
    public static String formatInput(String userInput) {
        StringBuilder formattedInput = new StringBuilder(userInput);
        formattedInput.insert(userInput.length() -1, ' ');
        return formattedInput.toString();
    }

    public static boolean luhnCheck(String userInput) {

        // The Luhn algorithm
        int totalSum = 0;
        for (int i = 0; i < userInput.length(); i++) {
            // The "- '0'" is used to format the integer
            int number = userInput.charAt(i) - '0';

            if (i % 2 == 0) {
                number = number * 2;
            }

            // Here we calculate the expected last digit
            if (i == userInput.length() - 1) {
                expectedCheckDigit = 10 - (totalSum % 10);
            }

            // This is needed to handle 2 digit number cases
            totalSum += number / 10;
            totalSum += number % 10;
        }

        if (totalSum % 10 == 0) {
            return true;
        }
        else {
            return false;
        }
    }

    // Dummy method to provide additional functionality
    // and thereby generate additional test cases.
    public String[] dummyMethod(String[] inputStringArray) {
        String[] answerStringArray = new String[]{"Java", "JavaScript", "Dota 2", "CS"};

        if (inputStringArray.length <= 1) {
            throw new IllegalArgumentException();
        }
        return answerStringArray;
    }
}


