import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

class LuhnAlgorithmTest {
    LuhnAlgorithm lunhalgorithm = new LuhnAlgorithm();

    private String validTestString = "4242424242424242";
    private String invalidTestString = "8112189878";

    @Test
    void main() {
    }

    @Test
    void testFormatInput() {
        assertEquals("424242424242424 2", lunhalgorithm.formatInput(validTestString));
        assertEquals(16 , validTestString.length());
        String formattedString = lunhalgorithm.formatInput(validTestString);
        assertEquals(17, formattedString.length());

        assertEquals("811218987 8", lunhalgorithm.formatInput(invalidTestString));
        assertEquals(10 , invalidTestString.length());
        String formattedStringTwo = lunhalgorithm.formatInput(invalidTestString);
        assertEquals(11, formattedStringTwo.length());
    }

    @Test
    void testLuhnCheck() {
        assertTrue(true, validTestString);
        assertFalse(false, invalidTestString);
    }

    @Test
    void testDummyMethod() {
        String[] testStringArray = new String[]{"Java", "JavaScript", "Dota 2", "CS"};
        assertArrayEquals(testStringArray , lunhalgorithm.dummyMethod(testStringArray));

        String[] testStringArrayTwo = new String[]{"Test"};
        assertThrows(IllegalArgumentException.class, () -> lunhalgorithm.dummyMethod(testStringArrayTwo));

        assertEquals(4, lunhalgorithm.dummyMethod(testStringArray).length);
    }
}